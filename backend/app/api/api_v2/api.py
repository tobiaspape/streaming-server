from fastapi import APIRouter

from app.api.api_v2.endpoints import streaming, settings, overlay, system, ws

api_router = APIRouter()
api_router.include_router(streaming.router, prefix="/streaming", tags=["streaming"])
api_router.include_router(settings.router, prefix="/config", tags=["settings"])
api_router.include_router(overlay.router, prefix="/overlay", tags=["overlay"])
api_router.include_router(system.router, prefix="/system", tags=["system"])
api_router.include_router(ws.router, prefix="/ws", tags=["websocket"])