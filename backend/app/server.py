###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2021 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################

from multiprocessing import freeze_support
import uvicorn


def main():
    freeze_support()
    port = 8080
    # workers=5 to increase performance and loop='none' to 'make' it use
    # asyncio.ProactorEventLoop so we can use asyncio.create_subprocess_exec()
    # changed to asyncio
    uvicorn.run("app:app", host="0.0.0.0", port=port, workers=1, loop="asyncio")


if __name__ == "__main__":
    main()
