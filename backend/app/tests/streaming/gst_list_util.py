import gi, pprint
gi.require_version('Gst', '1.0')
from gi.repository import Gst


def gst_list():
    """
    I just want to find out what kind of gst plugins there are
    """
    Gst.init(None)
    reg = Gst.Registry.get()

    plugins = [p.get_name() for p in reg.get_plugin_list()]
    features = {p: [f.get_name()  for f in reg.get_feature_list_by_plugin(p)] for p in plugins }
    pprint.pprint(features)

    encs = {enc.get_name(): (enc.get_metadata('long-name'), enc.get_metadata('description')) for enc in reg.get_feature_list(Gst.ElementFactory) \
        if 'Codec/Encoder/Video' in enc.get_metadata('klass')}
    pprint.pprint(encs)
    return [features, encs]

if __name__ == '__main__':
    gst_list()
