import pytest
from app.app import app
from app.settings.settings import global_config


@pytest.fixture
def paths():
    return [route.path for route in app.routes]


@pytest.mark.parametrize(
    "default_route", ["/openapi.json", "/docs", "/docs/oauth2-redirect", "/redoc"]
)
def test_sanitycheck(default_route, paths):
    assert default_route in paths, "Default Starlette routes should not be broken"


@pytest.mark.parametrize(
    "route,reason", [("/ping", "Is-Alive route should be present")]
)
def test_basics(route, reason, paths):
    assert route in paths, reason


# fmt: off
@pytest.mark.parametrize("route,reason", [
    ("/APIv1/status", "The portal status endpoint should be present"),
    ("/APIv1/show_message", "The portal message callback should be present"),
    ("/APIv1/start_stream", "The portal start-streaming callback should be present"),
    ("/APIv1/update_stream", "The portal update-streaming-url callback should be present"),
    ("/APIv1/stop_stream", "The portal stop-streaming callback should be present"),
    ("/APIv1/start_receive", "The portal start-receving callback should be present"),
    ("/APIv1/update_receive", "The portal update-receive-url callback should be present"),
    ("/APIv1/stop_receive", "The portal stop-receving callback should be present"),
])
# fmt: on
def test_portal_api(route, reason, paths):
    assert route in paths, reason


# fmt: off
@pytest.mark.parametrize("route,reason", [
    ("/APIv1/status", "The portal status endpoint should be present"),
    ("/APIv1/show_message", "The portal message callback should be present"),
    ("/APIv1/start_stream", "The portal start-streaming callback should be present"),
    ("/APIv1/update_stream", "The portal update-streaming-url callback should be present"),
    ("/APIv1/stop_stream", "The portal stop-streaming callback should be present"),
    ("/APIv1/start_receive", "The portal start-receving callback should be present"),
    ("/APIv1/update_receive", "The portal update-receive-url callback should be present"),
    ("/APIv1/stop_receive", "The portal stop-receving callback should be present"),
])
# fmt: on
def test_portal_api(route, reason, paths):
    assert route in paths, reason


# fmt: off
@pytest.mark.parametrize("route,reason", [
    ('/api/v2/overlay/file', "The ... endpoint should be present"),
    ('/api/v2/overlay/file', "The ... endpoint should be present"),
    ('/api/v2/overlay/file', "The ... endpoint should be present"),
    ('/api/v2/overlay/hymn', "The ... endpoint should be present"),
    ('/api/v2/overlay/text', "The ... endpoint should be present"),
    ('/api/v2/overlay/text', "The ... endpoint should be present"),
    ('/api/v2/overlay/image', "The ... endpoint should be present"),
    ('/api/v2/overlay/image', "The ... endpoint should be present"),
])
# fmt: on
def test_overlay_api(route, reason, paths):
    assert route in paths, reason


# fmt: off
@pytest.mark.parametrize("route,reason", [
    ('/api/v2/config/', "The ... endpoint should be present"),
    ('/api/v2/config/{config_name}', "The ... endpoint should be present"),
])
# fmt: on
def test_settings_api(route, reason, paths):
    assert route in paths, reason


# fmt: off
@pytest.mark.parametrize("route,reason", [
    ('/api/v2/streaming/status', "The ... endpoint should be present"),
    ('/api/v2/streaming/start', "The ... endpoint should be present"),
    ('/api/v2/streaming/receive', "The ... endpoint should be present"),
    ('/api/v2/streaming/stop', "The ... endpoint should be present"),
])
# fmt: on
def test_streaming_api(route, reason, paths):
    assert route in paths, reason


# fmt: off
@pytest.mark.parametrize("route,reason", [
    ('/api/v2/system/status', "The general status endpoint should be present"),
    ('/api/v2/system/off', "The system shutdown callback should be present"),
    ('/api/v2/system/reboot', "The system shutdown callback should be present"),
])
# fmt: on
def test_system_api(route, reason, paths):
    assert route in paths, reason


@pytest.mark.skipif(
    "not global_config.PUBLISH_METRICS", reason="Not sensible without publishing"
)
def test_instrumentation(paths):
    assert "/metrics" in paths, "The Prometheus endpoint should be present"


def test_safety_check(paths):
    # fmt: off
    assert len(paths) == (1 # root
                        + 4 # Starlette etc.
                        + 31),\
    """\
    There should only be known routes in the App. If this test fails,
    either you have a new route, then adapt. Or you disabled one, or FastAPI changed.
    """
    # fmt: on
