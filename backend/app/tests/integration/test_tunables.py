import time
import os, sys
import pytest

from fastapi.testclient import TestClient
from app.helper import is_linux
from app.main_director.media_states import MediaStates


@pytest.fixture()
def client(request):
    from app.settings.settings import global_config
    orig = global_config.PUBLISH_METRICS 

    global_config.PUBLISH_METRICS = False
    from app.app import app

    with TestClient(app) as c:
        yield c
    global_config.PUBLISH_METRICS = orig
    app.main_director.stop_glib_mainloop()


def run(c, name, url_entry, payload=None, post=False, expected=None):
    if post:
        r = c.post("/api/v2/" + url_entry, params=payload)
    else:
        r = c.get("/api/v2/" + url_entry, params=payload)

    if not expected:
        assert r.status_code == 204, name
        return

    if isinstance(expected, dict):
        response = r.json()
    elif isinstance(expected, str):
        response = r.text
    else:
        response = r.content
    assert response == expected, name


def test_tunables_error_recovery(client):
    # ensure stream is stopped
    run(client, "Stream Stop", "streaming/stop", post=True)
    # Try setting volume to nonsensical a
    expected = {
        "body": "a",
        "detail": "1 validation error for StreamingDirector\nvolume\n  value is not a valid float (type=type_error.float)",
    }
    run(
        client,
        "Set non-valid thing",
        "config/StreamingDirector.volume",
        {"value": "a"},
        True,
        expected,
    )
    # Try setting non-existing config entry
    expected = {
        "body": "42",
        "detail": "ConfigClass not found",
    }
    run(
        client,
        "Set non-existing thing",
        "config/CompletelyUnexpectedKey.AndValue",
        {"value": 42},
        True,
        expected,
    )
    # There was an old way of setting stuff, assert it is not the way anyomre
    expected = {
        "body": "42",
        "detail": "Not a valid key",
    }
    run(client, "Set non-existing thing", "config/volume", {"value": 42}, True, expected)

    # fmt: off


def test_tunables_offline(client):
    # ensure stream is stopped
    run(client, "Stream Start", "streaming/stop", post=True)

    # fmt: off
    # WITHOUT STREAMING
    run(client, "Stream Status", "streaming/status", expected={"status": MediaStates.NULL})

    run(client, "Set Volume", "config/StreamingDirector.volume", {"value": 6}, True, {"return": True})
    run(client, "Get Volume", "config/StreamingDirector.volume", expected={"return": 6})
    run(client, "Set Volume", "config/StreamingDirector.volume", {"value": 5}, True, {"return": True})
    run(client, "Get Volume", "config/StreamingDirector.volume", expected={"return": 5})

    run(client, "Set wb_red", "config/StreamingDirector.wb_red", {"value": 1}, True, {"return": True})
    run(client, "Get wb_red", "config/StreamingDirector.wb_red", expected={"return": 1})
    run(client, "Set wb_red", "config/StreamingDirector.wb_red", {"value": 0}, True, {"return": True})
    run(client, "Get wb_red", "config/StreamingDirector.wb_red", expected={"return": 0})

    run(client, "Set wb_green", "config/StreamingDirector.wb_green", {"value": 1}, True, {"return": True})
    run(client, "Get wb_green", "config/StreamingDirector.wb_green", expected={"return": 1})
    run(client, "Set wb_green", "config/StreamingDirector.wb_green", {"value": 0}, True, {"return": True})
    run(client, "Get wb_green", "config/StreamingDirector.wb_green", expected={"return": 0})

    run(client, "Set wb_blue", "config/StreamingDirector.wb_blue", {"value": 1}, True, {"return": True})
    run(client, "Get wb_blue", "config/StreamingDirector.wb_blue", expected={"return": 1})
    run(client, "Set wb_blue", "config/StreamingDirector.wb_blue", {"value": 0}, True, {"return": True})
    run(client, "Get wb_blue", "config/StreamingDirector.wb_blue", expected={"return": 0})

    run(client, "Set video_bitrate", "config/StreamingDirector.video_bitrate", {"value": 1000000}, True, {"return": True})
    run(client, "Get video_bitrate", "config/StreamingDirector.video_bitrate", expected={"return": 1000000})
    run(client, "Set video_bitrate", "config/StreamingDirector.video_bitrate", {"value": 1000001}, True, {"return": True})
    run(client, "Get video_bitrate", "config/StreamingDirector.video_bitrate", expected={"return": 1000001})

    run(client, "Get default_hymn_book", "config/Overlay.default_hymn_book", expected={"return": "de_gesangbuch"})
    run(client, "Set default_hymn_book", "config/Overlay.default_hymn_book", {"value": "nl_gesangbuch"}, True, {"return": True})
    run(client, "Get default_hymn_book", "config/Overlay.default_hymn_book", expected={"return": "nl_gesangbuch"})
    run(client, "Set default_hymn_book", "config/Overlay.default_hymn_book", {"value": "de_gesangbuch"}, True, {"return": True})
    run(client, "Get default_hymn_book", "config/Overlay.default_hymn_book", expected={"return": "de_gesangbuch"})

def test_tunables_online(client):
    # ensure stream is STARTED
    run(client, "Stream Start", "streaming/start", post=True)
    # Starting a GStreamer pipeline usually takes a few milliseconds, and in some
    # cases the following status request will be faster than pipeline startup.
    # Work around the race condition by specifying ample sleep time.
    time.sleep(1)

    # fmt: off
    # WITH STREAMING
    run(client, "Stream Status", "streaming/status", expected={'status': MediaStates.PLAYING})

    run(client, "Set Volume", "config/StreamingDirector.volume", {"value": 6}, True, {'return': True})
    run(client, "Get Volume", "config/StreamingDirector.volume", expected={'return': 6.0})
    run(client, "Set Volume", "config/StreamingDirector.volume", {"value": 5}, True, {'return': True})
    run(client, "Get Volume", "config/StreamingDirector.volume", expected={'return': 5.0})

    run(client, "Set wb_red", "config/StreamingDirector.wb_red", {"value": 1}, True, {'return': True})
    run(client, "Get wb_red", "config/StreamingDirector.wb_red", expected={'return': 1.0})
    run(client, "Set wb_red", "config/StreamingDirector.wb_red", {"value": 0}, True, {'return': True})
    run(client, "Get wb_red", "config/StreamingDirector.wb_red", expected={'return': 0.0})

    run(client, "Set wb_green", "config/StreamingDirector.wb_green", {"value": 1}, True, {'return': True})
    run(client, "Get wb_green", "config/StreamingDirector.wb_green", expected={'return': 1.0})
    run(client, "Set wb_green", "config/StreamingDirector.wb_green", {"value": 0}, True, {'return': True})
    run(client, "Get wb_green", "config/StreamingDirector.wb_green", expected={'return': 0.0})

    run(client, "Set wb_blue", "config/StreamingDirector.wb_blue", {"value": 1}, True, {'return': True})
    run(client, "Get wb_blue", "config/StreamingDirector.wb_blue", expected={'return': 1.0})
    run(client, "Set wb_blue", "config/StreamingDirector.wb_blue", {"value": 0}, True, {'return': True})
    run(client, "Get wb_blue", "config/StreamingDirector.wb_blue", expected={'return': 0.0})

    run(client, "Set video_bitrate", "config/StreamingDirector.video_bitrate", {"value": 1000000}, True, {'return': True})
    run(client, "Get video_bitrate", "config/StreamingDirector.video_bitrate", expected={'return': 1000000})
    run(client, "Set video_bitrate", "config/StreamingDirector.video_bitrate", {"value": 1000001}, True, {'return': True})
    run(client, "Get video_bitrate", "config/StreamingDirector.video_bitrate", expected={'return': 1000001})

    # FIXME: nl_gesangbuch is not part of the database, but we do not check
    # for validity of default_hymn_book in the code right now. Maybe mark
    # the next two checks as xfail?
    run(client, "Set default_hymn_book", "config/Overlay.default_hymn_book", {"value": "nl_gesangbuch"}, True, {"return": True})
    run(client, "Get default_hymn_book", "config/Overlay.default_hymn_book", expected={"return": "nl_gesangbuch"})
    run(client, "Set default_hymn_book", "config/Overlay.default_hymn_book", {"value": "de_gesangbuch"}, True, {"return": True})
    run(client, "Get default_hymn_book", "config/Overlay.default_hymn_book", expected={"return": "de_gesangbuch"})

    run(client, "Set text overlay", "overlay/text", {"left": "", "right": "BLA"}, True, "null")
    run(client, "Set text overlay", "overlay/text", {"left": "BLUB", "right": "BLA"}, True, "null")
    # FIXME: This test should fail, but right now enforcement is done by the frontend instead of the backend
    run(client, "Set text overlay", "overlay/text", {"left": "BLUB", "right": ""}, True, "null")
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1"}, True, "null")
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1", "verse": ""}, True, "null")
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1", "verse": "4"}, True, "null")
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1234"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1234", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1234", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"verse": ""}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"verse": "4"}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set default_hymn_book", "config/Overlay.default_hymn_book", {"value": "xx_gesangbuch"}, True, {"return": True})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1234"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1234", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1234", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"verse": ""}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"verse": "4"}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set default_hymn_book", "config/Overlay.default_hymn_book", {"value": "de_gesangbuch"}, True, {"return": True})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "1"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "1", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "1", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "1234"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "1234", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "1234", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "", "verse": ""}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "hymn": "", "verse": "4"}, True, {"return": {"rc": 1, "errormsg": "Invalid book/hymn combination"}})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch"}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "verse": ""}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"book": "xx_gesangbuch", "verse": "4"}, True, {"detail": [{"loc": ["query", "hymn"], "msg": "field required", "type": "value_error.missing"}]})
    run(client, "Set hymn overlay", "overlay/hymn", {"hymn": "1"}, True, "null")
    # TODO: Check image overlays, standalone and with text

    # fmt: on
    # stop for good measure
    run(client, "Stream Stop", "streaming/stop", post=True)


if __name__ == "__main__":
    test_tunables()
