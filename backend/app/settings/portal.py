from pydantic import BaseModel
from . import settings_class


@settings_class
class PortalDirector(BaseModel):
    keypress_url: str = ""
    cloud_proxy: str = ""

    class Config:
        validate_assignment = True
