# setting class registry
_settings_classes = []


def register_class(cls):
    _settings_classes.append(cls)


def unregister_class(cls):
    if cls in _settings_classes:
        _settings_classes.remove(cls)


def settings_class(cls):
    register_class(cls)
    return cls


def settings_classes():
    return _settings_classes[:]
