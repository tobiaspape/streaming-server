#
import sys
from pathlib import Path

def is_linux():
  return sys.platform == 'linux'

def is_raspberry_pi():
  if not is_linux():
    return False

  dt = Path('/sys/firmware/devicetree/base/model')
  if dt.exists() and 'raspberry pi' in dt.read_text().lower():
    return True
  cpuinfo = Path('/proc/cpuinfo')
  if cpuinfo.exists() and 'raspberry pi' in cpuinfo.read_text().lower():
    return True

  return False
