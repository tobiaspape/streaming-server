# Hai-End Streaming Server - Backend

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Web App for Raspbery PI based Streaming Server

## Server API

* [System Service](doc/SystemService.md)
* [Conf Service](doc/ConfService.md)
* [Overlay Service](doc/OverlayService.md)
* [Streaming Service](doc/Streaming.md)
* [Janus Service](doc/Janus.md)

---

## Development Setup

### Raspberry PI: Requirements

Add additional system packages have to be installed:

```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python3-gst-1.0
sudo apt-get install gstreamer1.0-plugins-good gstreamer1.0-plugins-base gstreamer1.0-plugins-bad
sudo apt-get install gstreamer1.0-omx gstreamer1.0-libav gstreamer1.0-alsa gstreamer1.0-tools
sudo apt-get install frei0r-plugins i2c-tools xorg
sudo apt-get install python3-autobahn python3-pil python3-serial
```

**Note:** This component will be deployed to `/opt/streaming-server` on the PIs.

### Install Python libaries

Additional Python libraries are required.

```sh
pip3 install -r requirements.txt
```

### Alternative: Windows WSL2

**⚠ Limitation:**
**No audio/video connections are possible.**

Use [Debian](https://www.microsoft.com/de-de/search/shop/apps?q=wsl&devicetype=pc&category=Developer+tools&Price=0) for the Windows Subsystem for Linux (WSL).

Follow instructions described at Raspberry PI to install system packages and Python libs.

## Configuration of local dev Environment

* Edit the `conf.yml` and `logging.conf` files for your needs. However the server should run out of the box without editing the config files.
* Create the overlay_root directory, where the users' overlay pictures are saved. Per default `mkdir ../../overlay_img` (exec within `backend` dir)

## Load our custom gstreamer build (if you use one of our images)

```sh
export $(xargs < /opt/gst-build/gst-build/builddir/gst.env)
```

## Run Server
Execute this within the `backend` directory and be shure to compile the webui first, so that the `webui/dist` directory is present.

```sh
uvicorn --host 0.0.0.0 --port 8080 --workers 1 --loop asyncio app.app:app
```

Now the Web App could be loaded: [http://localhost:8080]

---

#### REST

* States should be send in String constants and addition meaningful english
  message text, e.g.:

  ```json
  {
    "state": "CONNECTION_FAILED",
    "message": "Connection to system failed."
  }
  ```

#### vue.js

The vue.js project has configured lint.

### Contributor

* Maik Ender, mail@maikender.de
* Carl-Daniel Hailfinger, carl-daniel@hailfinger.org
* Markus Leitold, markus.leitold@confisys.de
* Samuel Simmerling, samuel@simmerling.online
* ...
* //@TODO

## Licence
[AGPL-3.0](LICENCE)
