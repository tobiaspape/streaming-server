# Hai-End Streaming - WebUI

Vue.js based web UI for Hai-End Streaming Server.

## Development Requirements

* [Node.js](https://nodejs.org/en/) v14 (LTS version)

## Project setup

All following commands are executed in project root directory.

To install all required library dependencies:

```sh
npm install
```

Note: this has to be done only once. Or to update dependencies.

### Compiles and hot-reloads for development

```sh
npm run serve
```

The Frontend is available after starting at: [http://localhost:8080](http://localhost:8080)

### Compiles and minifies for production

```sh
npm run build
```

### Lints and fixes files

```sh
npm run lint
```

```sh
npm run lint:fix
```

We use [JavaScript Standard Style](https://standardjs.com/) for linting.

### Customize configuration

See file [`.env`](.env). To modify it, create a local copy `.env.local`

#### Variables

**VUE_APP_HAIEND_STREAMING_SERVER:**
Server-URL of Hai-End Streaming Server.

Default: `window.location.origin`

**VUE_APP_JANUS_SERVICE_SERVER:**

Server-URL of Janus Service.

Default: `http[s]://${window.location.hostname}:8088/janus`

**VUE_APP_JANUS_SERVICE_STUN_SERVER:**

Server-URL of STUN-Server requred by Janus Service.

Default: `stun:${window.location.hostname}`

---

See [Configuration Reference](https://cli.vuejs.org/config/).

## Development

* https://webrtcglossary.com/webrtc-internals/ -> chrome://webrtc-internals/

## Contributor

* Carl-Daniel Hailfinger
* Maik Ender
* Stephan Strittmatter
* Samuel Simmerling
* Markus Leitold

## License

[LICENSE](LICENSE)

List of used libraries:

* [vue.js](https://vuejs.org/), [MIT License](https://github.com/vuejs/vue/blob/dev/LICENSE)
* [vue-i18n](https://kazupon.github.io/vue-i18n/), [MIT License](https://github.com/kazupon/vue-i18n/blob/v8.x/LICENSE)
* [BootstrapVue](https://bootstrap-vue.org/), [MIT License](https://github.com/bootstrap-vue/bootstrap-vue/blob/master/LICENSE)
* [axios](https://axios-http.com/), [MIT License](https://github.com/axios/axios/blob/master/LICENSE)
* [dotenv](https://github.com/motdotla/dotenv), [BSD-2-Clause License](https://github.com/motdotla/dotenv/blob/master/LICENSE)
* [janus-gateway](https://github.com/meetecho/janus-gateway), [GPL-3.0 License](https://github.com/meetecho/janus-gateway/blob/master/COPYING)
