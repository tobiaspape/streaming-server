import supportedLocales from '@/config/supported-locale-list'

export function getSupportedLocales () {
  return supportedLocales
}

export function supportedLocalesInclude (locale) {
  return supportedLocales.some(language => language.code === locale)
}
