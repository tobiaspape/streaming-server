/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
 */
const mediaStates = {
  NULL: 'NULL',
  READY: 'READY',
  STARTING: 'STARTING',
  PLAYING: 'PLAYING',
  ERROR: 'ERROR',
  RECOVER: 'RECOVER',
  WS_CONNECTION_LOST: 'WS_CONNECTION_LOST'

}

export default mediaStates
