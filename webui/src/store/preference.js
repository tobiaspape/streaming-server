/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import { defineStore } from 'pinia'
import preferenceService from '@/services/preference.service'

export const usePreferenceStore = defineStore('preferences', {

  state: () => ({
    preferences: null
  }),
  getters: {
    get: (state) => (key, defaultValue) => {
      if (!state.preferences) {
        console.error('Preferences not loaded!')
      }
      if (state.preferences[key]) {
        return state.preferences[key]
      } else {
        return defaultValue
      }
    }
  },
  actions: {
    set (payload) {
      this.preferences[payload.key] = payload.value
      preferenceService.save(this.preferences)
    },
    setAll (preferences) {
      this.preferences = preferences
      preferenceService.save(this.preferences)
    },
    load () {
      this.preferences = preferenceService.load()
    }
  }
})
