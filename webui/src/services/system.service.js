/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import api from './api'

const systemService = {

  actionPowerOff () {
    api.restClient().post('/system/off')
      .then((response) => {
        if (response.status !== '200') {
          console.error(response)
        }
      })
  },

  actionReboot () {
    api.restClient().post('/system/reboot')
      .then((response) => {
        if (response.status !== '200') {
          console.error(response)
        }
      })
  },

  actionRestart () {
    api.restClient().post('/system/restart')
      .then((response) => {
        if (response.status !== '200') {
          console.error(response)
        }
      })
  }
}

export default systemService
