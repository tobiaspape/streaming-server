/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import api from './api'

const streamingService = {
  async startStream (flag) {
    flag = (flag === 'test')
    const response = await api.restClient().post(`/streaming/start?test=${flag}`)
    if (response.status === 200 || response.status === 204) {
      return true
    }
    console.error('start failed', response)
  },
  async stopStream () {
    const response = await api.restClient().post('/streaming/stop')
    if (response.status === 200 || response.status === 204) {
      return true
    }
    console.error('stop failed', response)
  },

  async receiveStream () {
    const response = await api.restClient().post('/streaming/receive')
    if (response.status === 200 || response.status === 204) {
      return true
    }
    console.error('receive failed', response)
  }
}

export default streamingService
