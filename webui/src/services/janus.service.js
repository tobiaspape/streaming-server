/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import { Janus } from 'janus-gateway'

const janusService = {
  janus: null,
  init () {
    Janus.init({
      debug: true,
      dependencies: Janus.useDefaultDependencies(),
      callback: () => {
        this.connect(this.getServer())
      }
    })
    this.janus = new Janus()
    // @TODO
  },
  getServer () {
    let server
    if (process.env.VUE_APP_JANUS_SERVICE_SERVER) {
      server = process.env.VUE_APP_JANUS_SERVICE_SERVER
    } else {
      if (window.location.protocol === 'http:') {
        server = `http://${window.location.hostname}:8088/janus`
      } else {
        server = `https://${window.location.hostname}:8089/janus`
      }
    }
    console.info(`%c[Hai-End-Streaming] 📹 Janus-Service: ${server}`, 'color: #5793c9')
    return server
  },
  getStunServer () {
    let server
    if (process.env.VUE_APP_JANUS_SERVICE_STUN_SERVER) {
      server = process.env.VUE_APP_JANUS_SERVICE_STUN_SERVER
    } else {
      server = `stun:${window.location.hostname}`
    }
    console.info(`%c[Hai-End-Streaming] ⛓️ Janus Stun-Service: ${server}`, 'color: #5793c9')
    return server
  }
}

export default janusService
