/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2024 Stephan Strittmatter
*/
import axios from 'axios'

const licenseService = {
  restClient () {
    return axios.create({
      timeout: 5000,
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },
  async getFrontendLicenses () {
    const response = await this.restClient().get('/frontend-licenses.json')
    if (response.status !== 200) {
      console.error('loading frontend licenses failed', response)
      return []
    }
    return response.data
  },
  async getBackendLicenses () {
    const response = await this.restClient().get('/backend-licenses.json')
    if (response.status !== 200) {
      console.error('loading frontend licenses failed', response)
      return []
    }
    return response.data
  }

}
export default licenseService
