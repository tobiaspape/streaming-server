/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import api from './api'

const confService = {
  async get (property) {
    const response = await api.restClient().get(`/config/${property}`)
    if (response.status === 200) {
      return response.data.return
    }
    console.error(response)
  },
  async set (property, value) {
    const response = await api.restClient().post(`/config/${property}?value=${value}`)
    if (response.status === 200) {
      return true
    }
    console.error(response)
  },
  async getStatus (property) {
    console.warn('getStatus is deprecated. Use vuex data "Config/serverStatus" provided by websocket.')
    const response = await api.restClient().get('/streaming/status')
    if (response.status === 200) {
      return response.data.status
    }
    console.error(response)
  },
  async getROProperties () {
    const response = await api.restClient().get('/config/')
    if (response.status === 200) {
      return response.data
    }
    console.error(response)
  },
  async setEndCardText (text) {
    const params = new URLSearchParams({ value: text })
    const response = await api.restClient().post(`/config/Overlay.end_card_text?${params}`)
    if (response.status === 200) {
      return true
    }
    console.error('setting end_card_text overlay failed', response)
  },
  async getEndCardText () {
    const response = await api.restClient().get('/config/Overlay.end_card_text')
    if (response.status === 200 && response.data) {
      return response.data.return
    }
    console.error('getting end_card_text overlay failed', response)
  }
}

export default confService
