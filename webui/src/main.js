/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import Vue from 'vue'
import { createPinia, PiniaVuePlugin } from 'pinia'
import VueI18n from 'vue-i18n'
import VueMeta from 'vue-meta'
import './plugins/bootstrap-vue'
import './plugins/vee-validate'
import './registerServiceWorker'
import App from './App.vue'
import router from './router'
import i18n from './plugins/vue-i18n'

Vue.config.productionTip = false
const pinia = createPinia()

Vue.use(VueI18n)
Vue.use(VueMeta)
Vue.use(PiniaVuePlugin)

new Vue({
  router,
  i18n,
  pinia,
  render: h => h(App)
}).$mount('#app')
